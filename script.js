// AJAX позволяет обращаться к серверу и делать http запросы в фоновом режиме без перезагрузки страницы.
// Это позволяет обеспечить плавную динамичность веб-страниц




fetch('https://swapi.dev/api/films/')
  .then(response => response.json())
  .then(data => {

    data.results.forEach(episode => {
      const container = document.createElement("div");
      const id = document.createElement("h3");
      const title = document.createElement("h1");
      const prehistory = document.createElement("p");
      const charactersList = document.createElement("ul");

      id.append(`${episode.episode_id} Episode `);
      title.append(`-------------------------- ${episode.title} --------------------------`);
      prehistory.append(`Prehistory: ${episode.opening_crawl}`);
      container.append(id, title, prehistory);

      document.body.append(container);

      episode.characters.forEach(character => {
        const charactersItem = document.createElement("li");

        fetch(character)
          .then(response => response.json())
          .then(data => {
            charactersItem.append(data.name)
            charactersList.append(charactersItem)
          })
      })
      container.append(charactersList);
    })
  })

